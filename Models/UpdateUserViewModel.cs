﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Models
{
    class UpdateUserViewModel
    {
        public string Name { get; set; }
        
        public string Country { get; set; }
    }
}
