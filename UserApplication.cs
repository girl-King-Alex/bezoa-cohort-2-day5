﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Day5.Interfaces;
using Day5.Models;
using System.Threading.Tasks;

namespace Day5
{
    class UserApplication
    {
        private readonly ILogger _logger;
        private readonly StringBuilder _stringBuilder;
        private readonly IMenu _menu;
        private readonly IService<User> _UserService;

       
        public UserApplication(ILogger Logger)
        {
           _logger =  Logger;
            _stringBuilder = new StringBuilder();
            _menu = new UserMenu(Logger);
            _UserService = new UserService();
        }

        public void Run()
        {
            var isRunning = true;

            mainmenu:
            _stringBuilder.AppendLine("Press: ");
            _stringBuilder.AppendLine("Press: ");
            _stringBuilder.AppendLine("1. Get All Users");
            _stringBuilder.AppendLine("2. Get All Users In A Particular Continent");
            _stringBuilder.AppendLine("3. Add A New User");
            _stringBuilder.AppendLine("4. Update An Existing User");
            _stringBuilder.AppendLine("5. Back to Main Menu");


            while (isRunning)
            {
                _menu.Display(_stringBuilder.ToString());
                switch (Console.ReadLine())
                {
                    case "1":
                        GetAllUsers();
                        DisplayPrompt();
                        if (Console.ReadLine() == "1")
                        {
                            goto mainmenu;
                        }
                        else
                        {
                            isRunning = false;
                            Application.Run();
                            break;
                        }

                    case "2":
                        GetAllUsersInAContinent();
                        DisplayPrompt();
                        if (Console.ReadLine() == "1")
                        {
                            goto mainmenu;
                        }
                        else
                        {
                            isRunning = false;
                        }
                        break;


                    case "3":
                        New();
                        DisplayPrompt();
                        if (Console.ReadLine() == "1")
                        {
                            goto mainmenu;
                        }
                        else
                        {
                            isRunning = false;
                        }
                        break;


                    case "4":
                        Update();
                        DisplayPrompt();
                        if (Console.ReadLine() == "1")
                        {
                            goto mainmenu;
                        }
                        else
                        {
                            isRunning = false;
                        }
                        break;

                    case "5":
                        Application.Run();
                        isRunning = false;
                        break;
                    default:
                         _logger.LogLine("\nInvalid input...\nTry Again!!\n");
                       
                        break;
                }
            }
        }

        private void GetAllUsers()
        {
            foreach (var keyValuePair in _UserService.GetAll())
            {
               
                FetchUsers(keyValuePair.Value);
                _logger.LogLine("-------------");
                
            }
        }

        private void GetAllUsersInAContinent()
        {
            while (true)
            {
                _menu.Display("Select a continent to display:\n1. Africa\n2. Europe\n3. Asia");

                switch (Console.ReadLine())
                {
                    case "1":
                        // _logger.LogLine("Banks In Africa:");
                        _logger.Log("Banks In Africa:");
                        FetchUsers(_UserService.GetAll("Africa"));
                        break;

                    case "2":
                        // _logger.LogLine("Banks In Europe:");
                        _logger.Log("Banks In Europe:");
                        FetchUsers(_UserService.GetAll("Europe"));
                        break;

                    case "3":
                        // _logger.LogLine("Banks In Asia:");
                        _logger.Log("Banks In Asia:");
                        FetchUsers(_UserService.GetAll("Asia"));
                        break;
                    default:
                        _logger.Log("Invalid Input!\nTry Again!");
                        break;
                }
            }

        }

        private void CreateNewUser(string continent)
        {
            var model = new CreateUserViewModel();
            ///  _logger.LogLine("Enter the New Value for Bank Name");
            _logger.Log("Enter the New Value for user FirstName");
            var Firstname = Console.ReadLine();
            _logger.Log("Enter the New Value for user LasttName");
            var Lastname = Console.ReadLine();
            var name = Firstname + " " + Lastname;
            if (!string.IsNullOrWhiteSpace(name))
                model.FirstName = name;
            else
                _logger.Log("Blank Inputs not allowed!\nTry Again!");

            //_logger.LogLine("Enter the New Value for Region");
            _logger.Log("Enter the New Value for Country");
            var country = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(country))
                model.Country = country;
            else
                _logger.Log("Blank Inputs not allowed!\nTry Again!");

            var length = _UserService.GetAll(continent).Count();
            _UserService.Add(continent, new User{ Id = length, Name = name, Country = country });
        }


        private void UpdateUser(string continent, int index)
        {
            var model = new UpdateUserViewModel();

            var UserToUpdate = _UserService.Get(continent, index);

           
             _logger.LogLine("Enter new value for Name or blank to Ignore");
            var firstname = Console.ReadLine();

           _logger.LogLine("Enter new value for Name or blank to Ignore");
            var lastname = Console.ReadLine();
            var name = firstname + lastname;

            if (!string.IsNullOrWhiteSpace(name))
            {
                model.Name = name;
                UserToUpdate.Name = model.Name;
            }

            _logger.LogLine("Enter the New Value for Region");
           
            var country = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(country)) return;
            model.Country = country;
            UserToUpdate.Country = model.Country;
        }


        private void New()
        {
            _menu.Display("Select the continent :\n1. African\n2. European\n3. Asian");

            switch (Console.ReadLine())
            {
                case "1":
                    CreateNewUser("African");
                    break;

                case "2":
                    CreateNewUser("European");
                    break;

                case "3":
                    CreateNewUser("Asian");
                    break;
                default:
                    _logger.Log("Invalid Input!\nTry Again!");
                    break;
            }

        }

        private void Update()
        {
            _menu.Display("Select the continent :\n1. Africa\n2. Europe\n3. Asia");

            switch (Console.ReadLine())
            {

                case "1":
                    _menu.Display("Select the country :\n Enter 0 - n in the order they appear");
                    FetchUsers(_UserService.GetAll("Africa"));

                    if (int.TryParse(Console.ReadLine(), out var input))
                    {
                        UpdateUser("Africa", input);
                    }
                    else
                    {
                        _logger.Log("Invalid Input!\nTry Again!");
                    }
                    break;

                case "2":
                    _menu.Display("Select the country :\n Enter 0 - n in the order they appear");
                    FetchUsers(_UserService.GetAll("Africa"));

                    if (int.TryParse(Console.ReadLine(), out var input1))
                    {
                        UpdateUser("Europe", input1);
                    }
                    else
                    {
                        _logger.Log("Invalid Input!\nTry Again!");
                    }
                    break;

                case "3":
                    _menu.Display("Select the country :\n Enter 0 - n in the order they appear");
                    FetchUsers(_UserService.GetAll("Africa"));

                    if (int.TryParse(Console.ReadLine(), out var input2))
                    {
                        UpdateUser("Asia", input2);
                    }
                    else
                    {
                        _logger.Log("Invalid Input!\nTry Again!");
                    }
                    break;

                default:
                    _logger.Log("Invalid Input!\nTry Again!");
                    break;
            }

        }


        private void DisplayPrompt()
        {
            _menu.Display("Go to Bank Menu ?\n1. Yes\n2. Any other Key to go back main menu!");
        }

        private void FetchUsers(IEnumerable<User> Users)
        {
            foreach (var User in Users)
            {
               _logger.Log($"Id: {User.Id}\nName: {User.Name}\nRegion: {User.Country}\n");
            }
        }
    }
}
