﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day5.Interfaces;
using Day5.DataAccess;
using Day5.Helpers;

namespace Day5
{
    class UserService : IService<User>
    {
        private readonly DataStore database;
        private readonly ILogger _logger = new Logger();
        public UserService ()
        {
            database = new DataStore();
        }
        public void Add(string key,User entity)
        {
            if (database.Users.ContainsKey(key))
            {
                database.Users[key].Add(entity);
            }
            else
            {
                _logger.Log("Operation Failed!, Key does not exist!");
            }
        }

        public void Update(string key, int index, User entity)
        {
            if (database.Users.ContainsKey(key))
            {
                database.Users[key][index] = entity;
            }
            else
            {
                _logger.Log("Operation Failed!, Key does not exist!");
            }
        }

        public User Get(string key, int index)
        {
            return database.Users.ContainsKey(key) ? database.Users[key][index] : null;
        }

        public Dictionary<string, IList<User>> GetAll()
        {
            return database.Users;
        }

        public IEnumerable<User> GetAll(string key)
        {
            return database.Users.ContainsKey(key) ? database.Users[key] : null;
        }
    }
}
